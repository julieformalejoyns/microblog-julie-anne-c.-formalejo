<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<!-- HomePage CSS-->
<style type="text/css">
    .image-upload > input {
        display: none;
    }

    .attach-doc {
        cursor: pointer;
    }

    * {
        font-family: Arial, Helvetica, sans-serif;
        margin: 0;
    }

    .header {
        padding: 80px;
        text-align: center;
        background: #1abc9c;
        color: white;
    }

    .header h1 {
        font-size: 40px;
    }

    .navbar {
        overflow: hidden;
        background-color: #333;
    }

    .navbar a {
        float: left;
        display: block;
        color: white;
        text-align: center;
        padding: 14px 20px;
        text-decoration: none;
    }

    .navbar a.right {
        float: right;
    }

    .navbar a:hover {
        background-color: #ddd;
        color: black;
    }

    .row {  
        display: -ms-flexbox; 
        display: flex;
        -ms-flex-wrap: wrap; 
        flex-wrap: wrap;
    }

    .side {
        -ms-flex: 20%;
        flex: 20%;
        background-color: white;
        padding: 20px;
    }

    .main {   
        -ms-flex: 40%;
        flex: 40%;
        background-color: #f1f1f1;
        padding: 20px;
        margin-bottom: 50px;
    }

    .fakeimg {
        background-color: #aaa;
        width: 100%;
        padding: 20px;
    }

    .footer {
        padding: 20px;
        text-align: center;
        background: #ddd;
    }


.twPc-div {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #e1e8ed;
    border-radius: 6px;
    height: 230px;
    max-width: 500px; // orginal twitter width: 290px;
}
.twPc-bg {
    background-image: url("https://pbs.twimg.com/profile_banners/50988711/1384539792/600x200");
    background-position: 0 50%;
    background-size: 100% auto;
    border-bottom: 1px solid #e1e8ed;
    border-radius: 4px 4px 0 0;
    height: 95px;
    width: 100%;
}
.twPc-block {
    display: block !important;
}
.twPc-button {
    margin: -35px -10px 0;
    text-align: right;
    width: 100%;
}
.twPc-avatarLink {
    background-color: #fff;
    border-radius: 6px;
    display: inline-block !important;
    float: left;
    margin: -30px 5px 0 8px;
    max-width: 100%;
    padding: 1px;
    vertical-align: bottom;
}
.twPc-avatarImg {
    border: 2px solid #fff;
    border-radius: 7px;
    box-sizing: border-box;
    color: #fff;
    height: 72px;
    width: 72px;
}
.twPc-divUser {
    margin: 5px 0 0;
}
.twPc-divName {
    font-size: 18px;
    font-weight: 700;
    line-height: 21px;
}
.twPc-divName a {
    color: inherit !important;
}
.twPc-divStats {
    margin-left: 11px;
    padding: 10px 0;
}
.twPc-Arrange {
    box-sizing: border-box;
    display: table;
    margin: 0;
    min-width: 100%;
    padding: 0;
    table-layout: auto;
}
ul.twPc-Arrange {
    list-style: outside none none;
    margin: 0;
    padding: 0;
}
.twPc-ArrangeSizeFit {
    display: table-cell;
    padding: 0;
    vertical-align: top;
}
.twPc-ArrangeSizeFit a:hover {
    text-decoration: none;
}
.twPc-StatValue {
    display: block;
    font-size: 18px;
    font-weight: 500;
    transition: color 0.15s ease-in-out 0s;
}
.twPc-StatLabel {
    color: #8899a6;
    font-size: 10px;
    letter-spacing: 0.02em;
    overflow: hidden;
    text-transform: uppercase;
    transition: color 0.15s ease-in-out 0s;
}
</style>
<!-- HomePage -->
<div class="row">
    <div class="side">
        <a href="homepage" class="sidebar-option" style="text-decoration: none;">
            <h4 style="font-family: Arial; color: #cf352e;"><i class="fa fa-home" style="color: #cf352e;"></i>&nbsp;Home</h4>
        </a>
        <a href="explore" class="sidebar-option" style="text-decoration: none;">
            <h4 style="font-family: Arial; color: #cf352e;"><i class="fa fa-hashtag" style="color: #cf352e;"></i>&nbsp;Explore</h4>
        </a>
        <a href="notifications" class="sidebar-option" style="text-decoration: none;">
            <h4 style="font-family: Arial; color: #cf352e;"><i class="fa fa-bell" style="color: #cf352e;"></i>&nbsp;Notifications</h4>
        </a>
        <a href="profile" class="sidebar-option" style="text-decoration: none;">
            <h4 style="font-family: Arial; color: #cf352e;"><i class="fa fa-user" style="color: #cf352e;"></i>&nbsp;&nbsp;Profile</h4>
        </a>
        <a href="/" class="sidebar-option" style="text-decoration: none;">
            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-sign-out', 'style' => 'font-size: 28px; color: #cf352e;']), ['action' => 'logout'], ['escape' => false]) ?>
        </a>
        <br><br>
        <button id="myBtn" onclick="viewNewPostModal();">Post</button>
    </div>
    <div class="main">
        <div class="card">
            <div class="card-body">
<div class="twPc-div">
    <a class="twPc-bg twPc-block"></a>

    <div>
        <div class="twPc-button">
            <!-- Twitter Button | you can get from: https://about.twitter.com/tr/resources/buttons#follow -->
            <button id="btn btn-sm">Edit Profile</button>
        </div>

        <a title="Mert S. Kaplan" href="https://twitter.com/mertskaplan" class="twPc-avatarLink">
            <img alt="Mert S. Kaplan" src="https://mertskaplan.com/wp-content/plugins/msk-twprofilecard/img/mertskaplan.jpg" class="twPc-avatarImg">
        </a>

        <div class="twPc-divUser">
            <div class="twPc-divName">
                <a href="">Mert S. Kaplan</a>
            </div>
            <span>
                <a href="https://twitter.com/mertskaplan">@<span>mertskaplan</span></a>
            </span>
        </div>

        <div class="twPc-divStats">
            <ul class="twPc-Arrange">
                <li class="twPc-ArrangeSizeFit">
                    <a href="https://twitter.com/mertskaplan" title="9.840 Tweet">
                        <span class="twPc-StatLabel twPc-block">Tweets</span>
                        <span class="twPc-StatValue">9.840</span>
                    </a>
                </li>
                <li class="twPc-ArrangeSizeFit">
                    <a href="followinglist" title="885 Following">
                        <span class="twPc-StatLabel twPc-block">Following</span>
                        <span class="twPc-StatValue">885</span>
                    </a>
                </li>
                <li class="twPc-ArrangeSizeFit">
                    <a href="followerlist" title="1.810 Followers">
                        <span class="twPc-StatLabel twPc-block">Followers</span>
                        <span class="twPc-StatValue">1.810</span>
                    </a>
                </li>
            </ul>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h3>My Following List</h3>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
            </div>
        </div> 
    </div>
    <div class="side">
        <div class="list-group-item">
            <h3>Trends for You</h3>
                <hr style="border-top: 1px solid red;">
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-star" style="color: #cf352e;"></i>&nbsp;Sakura</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.5m Tweets</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-star" style="color: #cf352e;"></i>&nbsp;Miss Universe</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;700k Tweets</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-star" style="color: #cf352e;"></i>&nbsp;Occidental Mindoro</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;300k Tweets</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-star" style="color: #cf352e;"></i>&nbsp;Jeongyeon</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;250k Tweets</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-star" style="color: #cf352e;"></i>&nbsp;LS1 Era</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;200k Tweets</h6>
                </a>
        </div>
        <br>
        <div class="list-group-item">
            <h3>Who to follow</h3>
                <hr style="border-top: 1px solid red;">
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-plus-circle" style="color: #cf352e;"></i>&nbsp;LISA</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@lisabpofficial</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-plus-circle" style="color: #cf352e;"></i>&nbsp;ROSE</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@rosebpofficial</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-plus-circle" style="color: #cf352e;"></i>&nbsp;JENNIE</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@jenniebpofficial</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-plus-circle" style="color: #cf352e;"></i>&nbsp;JISOO</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@jisoobpofficial</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-plus-circle" style="color: #cf352e;"></i>&nbsp;BLACKPINK</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@blackpinkofficial</h6>
                </a>
        </div>
    </div>
</div>

<!-- Add Post -->
<div id="modalNewPost" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #cf352e;">New Post</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <?= $this->Form->create()?>
            <div class="modal-body">
                <div class="row NewPostValidations">
                    <div class="col-md-12">
                        <fieldset style="margin: 2px;border: 1px dotted #CCC;padding: 8px;margin-top: 0px;width: 100%;">
                        <legend style="border: none;margin-bottom: 0px;font-size: 15px; font-weight: normal;width:110px;">&nbsp;&nbsp;Post Details &nbsp;&nbsp;</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea class="form-control input-md input-flat" id="newPost" name="newPost" rows="4" autocomplete="off" maxlength="140"></textarea>
                                    <span id="count">Count :</span>
                                    <div class="image-upload">
                                        <label for="file-input">
                                            <i class="attach-doc fa fa-image fa-1x" aria-hidden="true" style="color: #cf352e;"></i>
                                        </label>
                                            <input id="file-input" type="file"/>
                                    </div>
                                    <div class="gallery" style="width: 350px;"></div>
                                </div>
                            </div>    
                        </fieldset>
                    </div>
                </div>
            </div>
        <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
            <?= $this->Form->button('Post', array('type' => 'button', 'class' => 'btn btn-success btn-sm btnAddPost', 'escape' => false)); ?>
            <button class="btn btn-danger bt-sm" data-dismiss="modal"> Close</button>
        </div>
        <?= $this->Form->end() ?>
    </div>
    </div>
</div>

<!-- Edit Post -->
<div id="modalEditPost" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #cf352e;">Edit Post</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <?= $this->Form->create()?>
            <div class="modal-body">
                <div class="row EditPostValidations">
                    <div class="col-md-12">
                        <input type="hidden" id="EditPostID" name="EditPostID">
                        <textarea class="form-control input-md input-flat" id="updatePost" name="updatePost" rows="4" autocomplete="off" maxlength="140"></textarea>
                        <span id="updatecount">Count :</span>
                        <div class="image-upload">
                            <label for="file-input">
                                <i class="attach-doc fa fa-image fa-1x" aria-hidden="true" style="color: #cf352e;"></i>
                            </label>
                                <input id="updatefile-input" type="file"/>
                        </div>
                        <div class="updategallery" style="width: 350px;"></div>
                    </div>
                </div>
            </div>
        <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
            <?= $this->Form->button('Update', array('type' => 'button', 'class' => 'btn btn-success btn-sm btnEditPost', 'escape' => false)); ?>
                <button class="btn btn-danger bt-sm" data-dismiss="modal"> Close</button>                      
        </div>
        <?= $this->Form->end() ?>
    </div>
    </div>
</div>

<!-- Delete Post -->
<div id="modalDeletePost" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #cf352e;">Delete Post</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <?= $this->Form->create()?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" id="DeletePostID" name="DeletePostID">
                        <h3>This can't be undone and it will be removed from your profile and the timeline of any accounts that follow you.</h3>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
                <?= $this->Form->button('Delete', array('type' => 'button', 'class' => 'btn btn-success btn-sm btnDeletePost', 'escape' => false)); ?>
                <button class="btn btn-danger bt-sm" data-dismiss="modal"> Close</button>                      
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<!-- Retweet Post -->
<div id="modalRetweetPost" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #cf352e;">Retweet Post</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
        <?= $this->Form->create()?>
        <div class="modal-body">
            <div class="row RetweetPostValidations">
                <div class="col-md-12">
                    <input type="hidden" id="RetweetPostID" name="RetweetPostID">
                    <textarea class="form-control input-md input-flat" id="retweetPost" name="retweetPost" rows="4" autocomplete="off" maxlength="140" placeholder="Add Comment"></textarea>
                    <span id="updatecount">Count :</span>
                    <div class="image-upload">
                        <label for="file-input">
                            <i class="attach-doc fa fa-image fa-1x" aria-hidden="true" style="color: #cf352e;"></i>
                        </label>
                            <input id="updatefile-input" type="file"/>
                    </div>
                    <div class="updategallery" style="width: 350px;"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
            <?= $this->Form->button('Update', array('type' => 'button', 'class' => 'btn btn-success btn-sm btnRetweetPost', 'escape' => false)); ?>
                <button class="btn btn-danger bt-sm" data-dismiss="modal"> Close</button>                      
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<!-- View Post -->
<div id="modalViewPost" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #cf352e;">View Post</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" id="ViewPostID" name="ViewPostID">
                        <fieldset style="margin: 2px;border: 1px dotted #CCC;padding: 8px;margin-top: 0px;width: 100%;">
                        <legend style="border: none;margin-bottom: 0px;font-size: 15px; font-weight: normal;width:110px;">&nbsp;&nbsp;Post Details &nbsp;&nbsp;</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>User's Name: </h5>
                                    <div id="userPostName"></div>
                                    <h5>Content: </h5>
                                    <div id="userPostContent"></div>
                                    <h5>Pictures :</h5>
                                    <img src="#" alt="NoPictures" id="userPostPictures">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
                <button class="btn btn-danger bt-sm" data-dismiss="modal"> Close</button>                      
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        // add post //
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#file-input').on('change', function() {
            imagesPreview(this, 'div.gallery');
        });

        // edit post //
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#updatefile-input').on('change', function() {
            imagesPreview(this, 'div.updategallery');
        });
    });

    var a = document.getElementById("newPost");
        a.addEventListener("keyup",function(){
        document.getElementById("count").innerHTML = "Count :" + " "+ a.value.length;
    })

    var b = document.getElementById("updatePost");
        b.addEventListener("keyup",function(){
        document.getElementById("updatecount").innerHTML = "Count :" + " "+ a.value.length;
    })  

    function viewNewPostModal() {
        $('#modalNewPost').modal('show');
        $('#newPost').val("");
        $('#gallery').html("");
    }

    function viewEditPostModal(id, homepage) {
        $('#modalEditPost').modal('show');
        $('input[name="EditPostID"]').val(id);
        $('[name="updatePost"]').val(posts[homepage]['content']);

    }

    function viewRetweetPostModal(id, homepage) {
        $('#modalRetweetPost').modal('show');
        $('input[name="RetweetPostID"]').val(id);
        $('[name="retweetPost"]').val(posts[homepage]['content']);
    }

    function viewRetweetPost(id, homepage) {
        $('#modalRetweetPost').modal('show') {
         $('input[name="spost_id"]').val(id);
        fullname = posts[homepage]['user']['first_name'] + ' ' + posts[homepage]['user']['last_name'];
        $('#userPostPictures').attr('src', posts[homepage]['user']['profile_image']);
        $('#userPostName').html(nickname);
        $('#userPostContent').html((posts[homepage]['content']).replace(/\n/g, '<br/>'));
    }

    $('.btnAddPost').on('click', function() {
        $.ajax({
        method: 'POST',
        url: '<?= $this->Url->build(['controller' => 'Posts', 'action' => 'add'])?>',
        data: {
            content: $('[name="newPost"]').val()
        },
        headers: {
            'X-CSRF-Token': $('[name="_csrfToken"]').val()
        },
        success: function(response) {
            $('#modalNewPost').modal('hide');
            response = JSON.parse(response);

            if (!response['response'] && response['error'] !== null) {
                error = response['error'];
                error_message = error['newPost']['_empty'];
                
                $('.NewPostValidations [name="newPost"]').closest('div').append('<small style="color:red;" class="post_req">' + error_message + '</small>');
            } else if (!response['response'] && response['error'] !== null) {
                location.reload();
            } else {
                location.reload();
            }
        }
        });
    });

    $('.btnEditPost').on('click', function() {
        $.ajax({
        method: 'POST',
        url: '<?= $this->Url->build(['controller' => 'Posts', 'action' => 'edit'])?>',
        data: {
            content: $('[name="updatePost"]').val(),
            posts_id: $('input[name="EditPostID"]').val()
        },
        headers: {
            'X-CSRF-Token': $('[name="_csrfToken"]').val()
        },
        success: function(response) {
            $('#modalEditPost').waitMe('hide');
            response = JSON.parse(response);

            if (!response['response'] && response['error'] !== null) {
                error = response['error'];
                error_message = error['content']['_empty'];
                
                $('.EditPostValidations [name="content"]').closest('div').append('<small style="color:red;" class="epost_req">' + error_message + '</small>');
            } else if (!response['response'] && response['error'] !== null) {
                location.reload();
            } else {
                location.reload();
            }
        }
        });
    });

    $('.btnRetweetPost').on('click', function() {
        content = $('[name="retweetPost"]').val();
        id =  $('input[name="RetweetPostID"]').val();
        retweetPostContent(id, content, '');
    });

    function retweetPostContent(id, content, action) {
        $.ajax({
        method: 'POST',
        url: '<?= $this->Url->build(['controller' => 'Posts', 'action' => 'add'])?>',
        data: {
            content: content,
            posts_id: id,
            action: action
        },
        headers: {
            'X-CSRF-Token': $('[name="_csrfToken"]').val()
        },
        success: function(response) {
            response = JSON.parse(response);

            if (!response['response'] && response['error'] !== null) {
                error = response['error'];
                error_message = error['content']['_empty'];
                
                $('.RetweetPostValidations [name="retweetPost"]').closest('div').append('<small style="color:red;" class="spost_req">' + error_message + '</small>');
            } else if (!response['response'] && response['error'] !== null) {
                location.reload();
            } else {
                location.reload();
            }
        }
        });
    }
</script>