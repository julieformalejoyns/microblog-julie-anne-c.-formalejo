<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<div class="container">
    <div class="row">
        <div class="col-md-5 mx-auto">
            <div id="first">
                <div class="myform form">
                    <div class="logo mb-3">
                        <div class="col-md-12 text-center">
                            <h1>Login</h1>
                        </div>
                    </div>
                    <?= $this->Flash->render() ?>
                    <form action="" method="post" name="login">
                        <?= $this->Form->create()?>
                        <div class="form-group">
                            <?= $this->Form->control('username', [
                                'label' => 'Username:',
                                'required' => true,
                                'placeholder' => 'Username'
                            ]) ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('password', [
                                'label' => 'Password:',
                                'required' => true,
                                'placeholder' => 'Password'
                            ]) ?>
                        </div>
                        <div class="form-group">
                            <p class="text-center" style="font-size: 13px;">Don't have account? <a href="#" id="signup">Sign up here</a></p>
                        </div> 
                        <div class="col-md-12 text-center mb-3">
                            <?= $this->Form->submit(__('Login'), ['class' => 'btn btn-block mybtn btn-primary tx-tfm']); ?>
                            <?= $this->Form->end() ?>
                        </div>
                    </form>
                </div>
            </div>
            <div id="second">
                <div class="myform form">
                    <div class="logo mb-3">
                        <div class="col-md-12 text-center">
                            <h1>Signup</h1>
                        </div>
                    </div>
                    <?= $this->Flash->render() ?>
                    <form action="#" name="registration">
                        <?= $this->Form->create()?>
                            <?php
                                echo $this->Form->control('username', [
                                    'label' => 'Username:',
                                    'placeholder' => 'Username',
                                    'autocomplete' => 'off',
                                    'required' => 'true'
                                ]);
                                echo $this->Form->control('first_name', [
                                    'label' => 'First Name:',
                                    'placeholder' => 'First Name',
                                    'autocomplete' => 'off',
                                    'required' => 'true'
                                ]);
                                echo $this->Form->control('last_name', [
                                    'label' => 'Last Name:',
                                    'placeholder' => 'Last Name',
                                    'autocomplete' => 'off',
                                    'required' => 'true'
                                ]);
                                echo $this->Form->control('email', [
                                    'label' => 'Email Address:',
                                    'placeholder' => 'Email Address',
                                    'autocomplete' => 'off',
                                    'required' => 'true'
                                ]);
                                echo $this->Form->control('password', [
                                    'label' => 'Password:',
                                    'placeholder' => 'Password',
                                    'autocomplete' => 'off',
                                    'required' => 'true'
                                ]);
                                echo $this->Form->control('confirm_password', [
                                    'label' => 'Confirm Password:',
                                    'placeholder' => 'Retype Password',
                                    'autocomplete' => 'off',
                                    'required' => 'true'
                                ]);
                            ?>
                        <br>    
                        <div class="form-group">
                            <p class="text-center" style="font-size: 13px;">Already have an account? <a href="#" id="signin">Sign up here</a></p>
                        </div> 
                        <div class="col-md-12 text-center mb-3">
                            <?= $this->Form->submit(__('Sign Up'), ['class' => 'btn btn-block mybtn btn-primary tx-tfm']); ?>
                            <?= $this->Form->end() ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
body {  
    padding-top:4.2rem;
    padding-bottom:4.2rem;
    background:rgba(0, 0, 0, 0.76);
}

a {
    text-decoration:none !important;
}

h1,h2,h3 {
    font-family: 'Kaushan Script', cursive;
}

.myform {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    padding: 1rem;
    -ms-flex-direction: column;
    flex-direction: column;
    width: 100%;
    pointer-events: auto;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: 1.1rem;
    outline: 0;
    max-width: 500px;
}

.tx-tfm {
    text-transform:uppercase;
}

.mybtn {
    border-radius:50px;
}
        
.login-or {
    position: relative;
    color: #aaa;
    margin-top: 10px;
    margin-bottom: 10px;
    padding-top: 10px;
    padding-bottom: 10px;
}

.span-or {
    display: block;
    position: absolute;
    left: 50%;
    top: -2px;
    margin-left: -25px;
    background-color: #fff;
    width: 50px;
    text-align: center;
}
         
.hr-or {
    height: 1px;
    margin-top: 0px !important;
    margin-bottom: 0px !important;
}

.google {
    color:#666;
    width:100%;
    height:40px;
    text-align:center;
    outline:none;
    border: 1px solid lightgrey;
}

form .error {
    color: #ff0000;
}

#second { 
    display:none;
}
</style>

<script type="text/javascript">
    $("#signup").click(function() {
    $("#first").fadeOut("fast", function() {
    $("#second").fadeIn("fast");
    });
});

    $("#signin").click(function() {
    $("#second").fadeOut("fast", function() {
    $("#first").fadeIn("fast");
    });
});
</script>