<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<!-- HomePage CSS-->
<style type="text/css">
    .image-upload > input {
        display: none;
    }

    .attach-doc {
        cursor: pointer;
    }

    * {
        font-family: Arial, Helvetica, sans-serif;
        margin: 0;
    }

    .header {
        padding: 80px;
        text-align: center;
        background: #1abc9c;
        color: white;
    }

    .header h1 {
        font-size: 40px;
    }

    .navbar {
        overflow: hidden;
        background-color: #333;
    }

    .navbar a {
        float: left;
        display: block;
        color: white;
        text-align: center;
        padding: 14px 20px;
        text-decoration: none;
    }

    .navbar a.right {
        float: right;
    }

    .navbar a:hover {
        background-color: #ddd;
        color: black;
    }

    .row {  
        display: -ms-flexbox; 
        display: flex;
        -ms-flex-wrap: wrap; 
        flex-wrap: wrap;
    }

    .side {
        -ms-flex: 20%;
        flex: 20%;
        background-color: white;
        padding: 20px;
    }

    .main {   
        -ms-flex: 40%;
        flex: 40%;
        background-color: #f1f1f1;
        padding: 20px;
        margin-bottom: 50px;
    }

    .fakeimg {
        background-color: #aaa;
        width: 100%;
        padding: 20px;
    }

    .footer {
        padding: 20px;
        text-align: center;
        background: #ddd;
    }


    @media screen and (max-width: 700px) {
        .row {   
            flex-direction: column;
        }
    }

    @media screen and (max-width: 400px) {
        .navbar a {
            float: none;
            width: 100%;
        }
    }   

    .profile-page .profile-header {
        box-shadow: 0 0 10px 0 rgba(183, 192, 206, 0.2);
        border: 1px solid #f2f4f9;
    }

    .profile-page .profile-header .cover {
        position: relative;
        border-radius: .25rem .25rem 0 0;
    }


    .profile-page .profile-header .cover figure {
        margin-bottom: 0;
    }

    @media (max-width: 767px) {
        .profile-page .profile-header .cover figure {
            height: 110px;
            overflow: hidden;
        }
    }

    @media (min-width: 2400px) {
        .profile-page .profile-header .cover figure {
            height: 280px;
            overflow: hidden;
        }
    }

    .profile-page .profile-header .cover figure img {
        border-radius: .25rem .25rem 0 0;
        width: 100%;
    }

    @media (max-width: 767px) {
        .profile-page .profile-header .cover figure img {
            -webkit-transform: scale(2);
            transform: scale(2);
            margin-top: 15px;
        }
    }

    @media (min-width: 2400px) {
        .profile-page .profile-header .cover figure img {
            margin-top: -55px;
        }
    }

    .profile-page .profile-header .cover .gray-shade {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 1;
        background: linear-gradient(rgba(255, 255, 255, 0.1), #fff 99%);
    }

    .profile-page .profile-header .cover .cover-body {
        position: absolute;
        bottom: -20px;
        left: 0;
        z-index: 2;
        width: 100%;
        padding: 0 20px;
    }

    .profile-page .profile-header .cover .cover-body .profile-pic {
        border-radius: 50%;
        width: 100px;
    }

    @media (max-width: 767px) {
        .profile-page .profile-header .cover .cover-body .profile-pic {
            width: 70px;
        }
    }

    .profile-page .profile-header .cover .cover-body .profile-name {
        font-size: 20px;
        font-weight: 600;
        margin-left: 17px;
    }

    .profile-page .profile-header .header-links {
        padding: 15px;
        display: -webkit-flex;
        display: flex;
        -webkit-justify-content: center;
        justify-content: center;
        background: #fff;
        border-radius: 0 0 .25rem .25rem;
    }

    .profile-page .profile-header .header-links ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
    }

    .profile-page .profile-header .header-links ul li a {
        color: #000;
        -webkit-transition: all .2s ease;
        transition: all .2s ease;
    }

    .profile-page .profile-header .header-links ul li:hover,
    .profile-page .profile-header .header-links ul li.active {
        color: #727cf5;
    }

    .profile-page .profile-header .header-links ul li:hover a,
    .profile-page .profile-header .header-links ul li.active a {
        color: #727cf5;
    }

    .profile-page .profile-body .left-wrapper .social-links a {
        width: 30px;
        height: 30px;
    }

    .profile-page .profile-body .right-wrapper .latest-photos > .row {
        margin-right: 0;
        margin-left: 0;
    }

    .profile-page .profile-body .right-wrapper .latest-photos > .row > div {
        padding-left: 3px;
        padding-right: 3px;
    }

    .profile-page .profile-body .right-wrapper .latest-photos > .row > div figure {
        -webkit-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
        margin-bottom: 6px;
    }

    .profile-page .profile-body .right-wrapper .latest-photos > .row > div figure:hover {
        -webkit-transform: scale(1.06);
        transform: scale(1.06);
    }

    .profile-page .profile-body .right-wrapper .latest-photos > .row > div figure img {
        border-radius: .25rem;
    }

    .rtl .profile-page .profile-header .cover .cover-body .profile-name {
        margin-left: 0;
        margin-right: 17px;
    }

    .img-xs {
        width: 37px;
        height: 37px;
    }

    .rounded-circle {
        border-radius: 50% !important;
    }

    img {
        vertical-align: middle;
        border-style: none;
    }

    .card-header:first-child {
        border-radius: 0 0 0 0;
    }

    .card-header {
        padding: 0.875rem 1.5rem;
        margin-bottom: 0;
        background-color: rgba(0, 0, 0, 0);
        border-bottom: 1px solid #f2f4f9;
    }

    .card-footer:last-child {
        border-radius: 0 0 0 0;
    }

    .card-footer {
        padding: 0.875rem 1.5rem;
        background-color: rgba(0, 0, 0, 0);
        border-top: 1px solid #f2f4f9;
    }

    .grid-margin {
        margin-bottom: 1rem;
    }

    .card {
        box-shadow: 0 0 10px 0 rgba(183, 192, 206, 0.2);
        -webkit-box-shadow: 0 0 10px 0 rgba(183, 192, 206, 0.2);
        -moz-box-shadow: 0 0 10px 0 rgba(183, 192, 206, 0.2);
        -ms-box-shadow: 0 0 10px 0 rgba(183, 192, 206, 0.2);
    }

    .rounded {
        border-radius: 0.25rem !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #f2f4f9;
        border-radius: 0.25rem;
    } 
</style>
<!-- HomePage -->
<div class="row">
    <div class="side">
        <a href="homepage" class="sidebar-option" style="text-decoration: none;">
            <h4 style="font-family: Arial; color: #cf352e;"><i class="fa fa-home" style="color: #cf352e;"></i>&nbsp;Home</h4>
        </a>
        <a href="explore" class="sidebar-option" style="text-decoration: none;">
            <h4 style="font-family: Arial; color: #cf352e;"><i class="fa fa-hashtag" style="color: #cf352e;"></i>&nbsp;Explore</h4>
        </a>
        <a href="notifications" class="sidebar-option" style="text-decoration: none;">
            <h4 style="font-family: Arial; color: #cf352e;"><i class="fa fa-bell" style="color: #cf352e;"></i>&nbsp;Notifications</h4>
        </a>
        <a href="profile" class="sidebar-option" style="text-decoration: none;">
            <h4 style="font-family: Arial; color: #cf352e;"><i class="fa fa-user" style="color: #cf352e;"></i>&nbsp;&nbsp;Profile</h4>
        </a>
        <a href="/" class="sidebar-option" style="text-decoration: none;">
            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-sign-out', 'style' => 'font-size: 28px; color: #cf352e;']), ['action' => 'logout'], ['escape' => false]) ?>
        </a>
        <br><br>
        <button id="myBtn" onclick="viewNewPostModal();">Post</button>
    </div>
    <div class="main">
        <div class="card">
            <div class="card-body">
                <div class="col-md-12 col-xl-12 middle-wrapper">
                    <div class="row">
                        <div class="col-md-12 grid-margin">
                            <div class="card rounded">
                                <div class="card-header">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="d-flex align-items-center">
                                            <img class="img-xs rounded-circle" src="https://bootdey.com/img/Content/avatar/avatar6.png" alt="">
                                                <div class="ml-2">
                                                    <p>Mike Popescu</p>
                                                    <p class="tx-11 text-muted">1 min ago</p>
                                                </div>
                                            </div>
                                            <div class="dropdown">
                                                <button class="btn p-0" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-horizontal icon-lg pb-3px">
                                                        <circle cx="12" cy="12" r="1"></circle>
                                                        <circle cx="19" cy="12" r="1"></circle>
                                                        <circle cx="5" cy="12" r="1"></circle>
                                                    </svg>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                                    <i class="fa fa-edit" style="font-size: 25px;"></i>
                                                    <span class="">Edit Post</span>
                                                </a>
                                                <a class="dropdown-item d-flex align-items-center" href="#">
                                                    <i class="fa fa-trash-o" style="font-size: 25px;"></i>
                                                    <span class="">Delete Post</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <p class="mb-3 tx-14">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus minima delectus nemo unde quae recusandae assumenda.</p>
                                    <img class="img-fluid" src="https://bootdey.com/img/Content/avatar/avatar6.png" alt="">
                                </div>
                                <div class="card-footer">
                                    <div class="d-flex post-actions">
                                        <a href="javascript:;" class="d-flex align-items-center text-muted mr-4">
                                            <i class="fa fa-comments-o" style="font-size: 25px;"></i>
                                        </a>
                                        <a href="javascript:;" class="d-flex align-items-center text-muted mr-4">
                                            <i class="fa fa-retweet" style="font-size: 25px;"></i>  
                                        </a>
                                        <a href="javascript:;" class="d-flex align-items-center text-muted">
                                            <i class="fa fa-heart" style="font-size: 25px;"></i> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <div class="side">
        <div class="list-group-item">
            <h3>Trends for You</h3>
                <hr style="border-top: 1px solid red;">
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-star" style="color: #cf352e;"></i>&nbsp;Sakura</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.5m Tweets</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-star" style="color: #cf352e;"></i>&nbsp;Miss Universe</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;700k Tweets</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-star" style="color: #cf352e;"></i>&nbsp;Occidental Mindoro</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;300k Tweets</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-star" style="color: #cf352e;"></i>&nbsp;Jeongyeon</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;250k Tweets</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-star" style="color: #cf352e;"></i>&nbsp;LS1 Era</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;200k Tweets</h6>
                </a>
        </div>
        <br>
        <div class="list-group-item">
            <h3>Who to follow</h3>
                <hr style="border-top: 1px solid red;">
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-plus-circle" style="color: #cf352e;"></i>&nbsp;LISA</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@lisabpofficial</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-plus-circle" style="color: #cf352e;"></i>&nbsp;ROSE</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@rosebpofficial</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-plus-circle" style="color: #cf352e;"></i>&nbsp;JENNIE</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@jenniebpofficial</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-plus-circle" style="color: #cf352e;"></i>&nbsp;JISOO</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@jisoobpofficial</h6>
                </a>
                <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                    <h4 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;"><i class="fa fa-plus-circle" style="color: #cf352e;"></i>&nbsp;BLACKPINK</h4>
                    <h6 style="font-family: Arial; color: #cf352e; line-height: 0.5rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@blackpinkofficial</h6>
                </a>
        </div>
    </div>
</div>

<!-- Add Post -->
<div id="modalNewPost" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #cf352e;">New Post</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <?= $this->Form->create()?>
            <div class="modal-body">
                <div class="row NewPostValidations">
                    <div class="col-md-12">
                        <fieldset style="margin: 2px;border: 1px dotted #CCC;padding: 8px;margin-top: 0px;width: 100%;">
                        <legend style="border: none;margin-bottom: 0px;font-size: 15px; font-weight: normal;width:110px;">&nbsp;&nbsp;Post Details &nbsp;&nbsp;</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea class="form-control input-md input-flat" id="newPost" name="newPost" rows="4" autocomplete="off" maxlength="140"></textarea>
                                    <span id="count">Count :</span>
                                    <div class="image-upload">
                                        <label for="file-input">
                                            <i class="attach-doc fa fa-image fa-1x" aria-hidden="true" style="color: #cf352e;"></i>
                                        </label>
                                            <input id="file-input" type="file"/>
                                    </div>
                                    <div class="gallery" style="width: 350px;"></div>
                                </div>
                            </div>    
                        </fieldset>
                    </div>
                </div>
            </div>
        <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
            <?= $this->Form->button('Post', array('type' => 'button', 'class' => 'btn btn-success btn-sm btnAddPost', 'escape' => false)); ?>
            <button class="btn btn-danger bt-sm" data-dismiss="modal"> Close</button>
        </div>
        <?= $this->Form->end() ?>
    </div>
    </div>
</div>

<!-- Edit Post -->
<div id="modalEditPost" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #cf352e;">Edit Post</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <?= $this->Form->create()?>
            <div class="modal-body">
                <div class="row EditPostValidations">
                    <div class="col-md-12">
                        <input type="hidden" id="EditPostID" name="EditPostID">
                        <textarea class="form-control input-md input-flat" id="updatePost" name="updatePost" rows="4" autocomplete="off" maxlength="140"></textarea>
                        <span id="updatecount">Count :</span>
                        <div class="image-upload">
                            <label for="file-input">
                                <i class="attach-doc fa fa-image fa-1x" aria-hidden="true" style="color: #cf352e;"></i>
                            </label>
                                <input id="updatefile-input" type="file"/>
                        </div>
                        <div class="updategallery" style="width: 350px;"></div>
                    </div>
                </div>
            </div>
        <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
            <?= $this->Form->button('Update', array('type' => 'button', 'class' => 'btn btn-success btn-sm btnEditPost', 'escape' => false)); ?>
                <button class="btn btn-danger bt-sm" data-dismiss="modal"> Close</button>                      
        </div>
        <?= $this->Form->end() ?>
    </div>
    </div>
</div>

<!-- Delete Post -->
<div id="modalDeletePost" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #cf352e;">Delete Post</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <?= $this->Form->create()?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" id="DeletePostID" name="DeletePostID">
                        <h3>This can't be undone and it will be removed from your profile and the timeline of any accounts that follow you.</h3>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
                <?= $this->Form->button('Delete', array('type' => 'button', 'class' => 'btn btn-success btn-sm btnDeletePost', 'escape' => false)); ?>
                <button class="btn btn-danger bt-sm" data-dismiss="modal"> Close</button>                      
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<!-- Retweet Post -->
<div id="modalRetweetPost" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #cf352e;">Retweet Post</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
        <?= $this->Form->create()?>
        <div class="modal-body">
            <div class="row RetweetPostValidations">
                <div class="col-md-12">
                    <input type="hidden" id="RetweetPostID" name="RetweetPostID">
                    <textarea class="form-control input-md input-flat" id="retweetPost" name="retweetPost" rows="4" autocomplete="off" maxlength="140" placeholder="Add Comment"></textarea>
                    <span id="updatecount">Count :</span>
                    <div class="image-upload">
                        <label for="file-input">
                            <i class="attach-doc fa fa-image fa-1x" aria-hidden="true" style="color: #cf352e;"></i>
                        </label>
                            <input id="updatefile-input" type="file"/>
                    </div>
                    <div class="updategallery" style="width: 350px;"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
            <?= $this->Form->button('Update', array('type' => 'button', 'class' => 'btn btn-success btn-sm btnRetweetPost', 'escape' => false)); ?>
                <button class="btn btn-danger bt-sm" data-dismiss="modal"> Close</button>                      
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<!-- View Post -->
<div id="modalViewPost" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #cf352e;">View Post</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" id="ViewPostID" name="ViewPostID">
                        <fieldset style="margin: 2px;border: 1px dotted #CCC;padding: 8px;margin-top: 0px;width: 100%;">
                        <legend style="border: none;margin-bottom: 0px;font-size: 15px; font-weight: normal;width:110px;">&nbsp;&nbsp;Post Details &nbsp;&nbsp;</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>User's Name: </h5>
                                    <div id="userPostName"></div>
                                    <h5>Content: </h5>
                                    <div id="userPostContent"></div>
                                    <h5>Pictures :</h5>
                                    <img src="#" alt="NoPictures" id="userPostPictures">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
                <button class="btn btn-danger bt-sm" data-dismiss="modal"> Close</button>                      
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        // add post //
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#file-input').on('change', function() {
            imagesPreview(this, 'div.gallery');
        });

        // edit post //
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#updatefile-input').on('change', function() {
            imagesPreview(this, 'div.updategallery');
        });
    });

    var a = document.getElementById("newPost");
        a.addEventListener("keyup",function(){
        document.getElementById("count").innerHTML = "Count :" + " "+ a.value.length;
    })

    var b = document.getElementById("updatePost");
        b.addEventListener("keyup",function(){
        document.getElementById("updatecount").innerHTML = "Count :" + " "+ a.value.length;
    })  

    function viewNewPostModal() {
        $('#modalNewPost').modal('show');
        $('#newPost').val("");
        $('#gallery').html("");
    }

    function viewEditPostModal(id, homepage) {
        $('#modalEditPost').modal('show');
        $('input[name="EditPostID"]').val(id);
        $('[name="updatePost"]').val(posts[homepage]['content']);

    }

    function viewRetweetPostModal(id, homepage) {
        $('#modalRetweetPost').modal('show');
        $('input[name="RetweetPostID"]').val(id);
        $('[name="retweetPost"]').val(posts[homepage]['content']);
    }

    function viewRetweetPost(id, homepage) {
        $('#modalRetweetPost').modal('show') {
         $('input[name="spost_id"]').val(id);
        fullname = posts[homepage]['user']['first_name'] + ' ' + posts[homepage]['user']['last_name'];
        $('#userPostPictures').attr('src', posts[homepage]['user']['profile_image']);
        $('#userPostName').html(nickname);
        $('#userPostContent').html((posts[homepage]['content']).replace(/\n/g, '<br/>'));
    }

    $('.btnAddPost').on('click', function() {
        $.ajax({
        method: 'POST',
        url: '<?= $this->Url->build(['controller' => 'Posts', 'action' => 'add'])?>',
        data: {
            content: $('[name="newPost"]').val()
        },
        headers: {
            'X-CSRF-Token': $('[name="_csrfToken"]').val()
        },
        success: function(response) {
            $('#modalNewPost').modal('hide');
            response = JSON.parse(response);

            if (!response['response'] && response['error'] !== null) {
                error = response['error'];
                error_message = error['newPost']['_empty'];
                
                $('.NewPostValidations [name="newPost"]').closest('div').append('<small style="color:red;" class="post_req">' + error_message + '</small>');
            } else if (!response['response'] && response['error'] !== null) {
                location.reload();
            } else {
                location.reload();
            }
        }
        });
    });

    $('.btnEditPost').on('click', function() {
        $.ajax({
        method: 'POST',
        url: '<?= $this->Url->build(['controller' => 'Posts', 'action' => 'edit'])?>',
        data: {
            content: $('[name="updatePost"]').val(),
            posts_id: $('input[name="EditPostID"]').val()
        },
        headers: {
            'X-CSRF-Token': $('[name="_csrfToken"]').val()
        },
        success: function(response) {
            $('#modalEditPost').waitMe('hide');
            response = JSON.parse(response);

            if (!response['response'] && response['error'] !== null) {
                error = response['error'];
                error_message = error['content']['_empty'];
                
                $('.EditPostValidations [name="content"]').closest('div').append('<small style="color:red;" class="epost_req">' + error_message + '</small>');
            } else if (!response['response'] && response['error'] !== null) {
                location.reload();
            } else {
                location.reload();
            }
        }
        });
    });

    $('.btnRetweetPost').on('click', function() {
        content = $('[name="retweetPost"]').val();
        id =  $('input[name="RetweetPostID"]').val();
        retweetPostContent(id, content, '');
    });

    function retweetPostContent(id, content, action) {
        $.ajax({
        method: 'POST',
        url: '<?= $this->Url->build(['controller' => 'Posts', 'action' => 'add'])?>',
        data: {
            content: content,
            posts_id: id,
            action: action
        },
        headers: {
            'X-CSRF-Token': $('[name="_csrfToken"]').val()
        },
        success: function(response) {
            response = JSON.parse(response);

            if (!response['response'] && response['error'] !== null) {
                error = response['error'];
                error_message = error['content']['_empty'];
                
                $('.RetweetPostValidations [name="retweetPost"]').closest('div').append('<small style="color:red;" class="spost_req">' + error_message + '</small>');
            } else if (!response['response'] && response['error'] !== null) {
                location.reload();
            } else {
                location.reload();
            }
        }
        });
    }
</script>