<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<body>
    <div class="container register" style="background: -webkit-linear-gradient(left, #8b0000, #cf352e);">
                <div class="row">
                    <div class="col-md-3 register-left">
                        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                        <h1 style="color: white; font-weight: bold;">Welcome</h1>
                        <p style="font-size: 13px; line-height: 1.0;">You are 30 seconds away from starting from your own post.</p>
                    </div>
                    <div class="col-md-9 register-right">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <h1 class="register-heading">Sign Up</h1>
                                <h3 class="register-heading">It's quick and easy.</h3>
                                <?= $this->Form->create() ?>
                                <div class="row register-form">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php
                                                echo $this->Form->control('username', ['label' => false, "placeholder" => "Username", 'autocomplete' => 'off', 'required' => true]);
                                                echo $this->Form->control('first_name', ['label' => false, "placeholder" => "First Name", 'autocomplete' => 'off', 'required' => true]);
                                                echo $this->Form->control('last_name', ['label' => false, "placeholder" => "Last Name", 'autocomplete' => 'off', 'required' => true]);
                                                echo $this->Form->control('email', ['label' => false, "placeholder" => "Email Address", 'autocomplete' => 'off', 'required' => true]);
                                                echo $this->Form->control('password', ['label' => false, "placeholder" => "Password", 'autocomplete' => 'off', 'required' => true]);
                                                echo $this->Form->control('confirm_password', ['label' => false, "placeholder" => "Confirm Password", 'autocomplete' => 'off', 'required' => true]);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <br><br><br>
                                            <?= $this->Form->button(__('Sign Up'), ['style' => 'background-color: #cf352e; width:50%; margin-left:25%; margin-right:25%;']) ?>
                                            <p style="color: #cf352e; text-align: center; font-size: 13px;">Don't have an account? <?= $this->Html->link(('Sign in'), ['action' => 'login'], ['style' => 'color: #cf352e; text-align: center; font-weight: bold;']) ?></p>
                                            <p style="color: black; text-align: center; line-height: 1.8; font-size: 13px;">By clicking signing up, means you agree to the <b>Terms and Coditions, Privacy Policy and Cookie Policy.</b> </p>
                                            <?= $this->Form->end() ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

</body>    
