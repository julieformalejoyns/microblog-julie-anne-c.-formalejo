<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Followersfollowing $followersfollowing
 * @var string[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $followersfollowing->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $followersfollowing->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Followersfollowing'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="followersfollowing form content">
            <?= $this->Form->create($followersfollowing) ?>
            <fieldset>
                <legend><?= __('Edit Followersfollowing') ?></legend>
                <?php
                    echo $this->Form->control('user_id', ['options' => $users, 'empty' => true]);
                    echo $this->Form->control('follow_id');
                    echo $this->Form->control('following_id');
                    echo $this->Form->control('is_deleted');
                    echo $this->Form->control('created_datetime', ['empty' => true]);
                    echo $this->Form->control('deleted_datetime', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
