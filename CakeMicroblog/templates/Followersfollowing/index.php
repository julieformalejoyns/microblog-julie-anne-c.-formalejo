<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Followersfollowing[]|\Cake\Collection\CollectionInterface $followersfollowing
 */
?>
<div class="followersfollowing index content">
    <?= $this->Html->link(__('New Followersfollowing'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Followersfollowing') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('follow_id') ?></th>
                    <th><?= $this->Paginator->sort('following_id') ?></th>
                    <th><?= $this->Paginator->sort('is_deleted') ?></th>
                    <th><?= $this->Paginator->sort('created_datetime') ?></th>
                    <th><?= $this->Paginator->sort('deleted_datetime') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($followersfollowing as $followersfollowing): ?>
                <tr>
                    <td><?= $this->Number->format($followersfollowing->id) ?></td>
                    <td><?= $followersfollowing->has('user') ? $this->Html->link($followersfollowing->user->id, ['controller' => 'Users', 'action' => 'view', $followersfollowing->user->id]) : '' ?></td>
                    <td><?= $this->Number->format($followersfollowing->follow_id) ?></td>
                    <td><?= $this->Number->format($followersfollowing->following_id) ?></td>
                    <td><?= h($followersfollowing->is_deleted) ?></td>
                    <td><?= h($followersfollowing->created_datetime) ?></td>
                    <td><?= h($followersfollowing->deleted_datetime) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $followersfollowing->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $followersfollowing->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $followersfollowing->id], ['confirm' => __('Are you sure you want to delete # {0}?', $followersfollowing->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
