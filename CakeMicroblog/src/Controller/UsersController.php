<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // $this->Authentication->addUnauthenticatedActions(['loginIndex', 'signup', 'verification']);
    }

    public function loginSignup()
    {
        $this->viewBuilder()->setLayout('login');
    }

    public function logout()
    {
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $this->Authentication->logout();

            return $this->redirect(['controller' => 'Users', 'action' => 'loginSignup']);
        }
    }

    public function signup()
    {
        $this->viewBuilder()->setLayout('signup');
        $user = $this->Users->newEntity($this->request->getData());
        $errors = $user->getErrors();
        
        if($errors) {
            echo json_encode(['error' => $errors, 'response' => false]);
            exit;
        } else {
            if ($this->request->is('post')) {
                $code = md5(strval(rand(0,1000)) . $user['email']);
                $user = $this->Users->patchEntity($user, $this->request->getData());
                $user['password_hash'] = $code;
                $user['profile_image'] = 'img/noprofile.png';
                if ($this->Users->save($user)) {

                    $link = 'localhost/CakeMicroblog/users/verification/' . base64_encode($code);

                    $mailer = new Mailer();
                    $mailer->setTransport('gmail')
                           ->setFrom('julieanneformalejo.yns@gmail.com')
                           ->setTo($user['email'])
                           ->setSubject('Microblog Account Activation')
                           ->setEmailFormat('both');
                    
                    $message = '<div id="' . rand(0, 2500) . '">'
                             .'Hello, ' . $user['first_name'] . ' ' . $user['last_name'] . '!'
                             . '<br/><br/>'
                             . 'Thank you for signing-up on our Microblog site! Please click on the "Verify Account!" button to validate your account for you to be able to officially log-in on your account.'
                             . '<br/><br/>'
                             . 'Thank you.'
                             . '<br/><br/>'
                             . '<a href="' . $link . '"><button class="btn btn-success">Verify Account!</button></a>'
                             . '<br/><br/>'
                             . '[' . date('H:i:s m/d/Y') . ']' . ' End of message.'
                             . '</div>';
                    

                    if ($mailer->deliver($message)) {
                        $this->Flash->success(__('Successfully registered! An email has been sent to validate and activate your account.'));
                        exit(json_encode(['error' => null, 'response' => true]));
                    } else {
                        $this->Flash->error(__('Account registration failed. Please, try again.'));
                        exit(json_encode(['error' => null, 'response' => false]));
                    }
                    
                } else {
                    $this->Flash->error(__('Account registration failed. Please, try again.'));
                    exit(json_encode(['error' => null, 'response' => false]));
                }
            }
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }


    public function verifyemail(){

    }

    public function profile()
    {
        $this->viewBuilder()->setLayout('homepage');
    }

    public function homepage()
    {
        $this->viewBuilder()->setLayout('homepage');
    }

    public function followerlist()
    {
        $this->viewBuilder()->setLayout('homepage');
    }

    public function followinglist()
    {
        $this->viewBuilder()->setLayout('homepage');
    }

    public function userfollowerlist()
    {
        $this->viewBuilder()->setLayout('homepage');
    }

    public function userfollowinglist()
    {
        $this->viewBuilder()->setLayout('homepage');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('homepage');
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Comments', 'FollowersFollowing', 'Notifications', 'PostLikes', 'Posts'],
        ]);

        $this->set(compact('user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
