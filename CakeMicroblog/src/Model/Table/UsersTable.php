<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\CommentsTable&\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\FollowersFollowingTable&\Cake\ORM\Association\HasMany $FollowersFollowing
 * @property \App\Model\Table\NotificationsTable&\Cake\ORM\Association\HasMany $Notifications
 * @property \App\Model\Table\PostLikesTable&\Cake\ORM\Association\HasMany $PostLikes
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\HasMany $Posts
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Comments', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('FollowersFollowing', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Notifications', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('PostLikes', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Posts', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 50)
            ->allowEmptyString('first_name')
            ->notEmptyString('firstname', 'Please fill up this field.');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 50)
            ->allowEmptyString('last_name')
            ->notEmptyString('firstname', 'Please fill up this field.');


        $validator
            ->email('email')
            ->allowEmptyString('email')
            ->notEmptyString('email', 'Please fill up this field.');

        $validator
            ->scalar('username')
            ->maxLength('username', 20)
            ->allowEmptyString('username')
            ->notEmptyString('username', 'Please fill up this field.');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->notEmptyString('password', 'Please fill this field.')
            ->add('password', 'error', 
                [
                    'rule' => function($value, $context){
                        $password = $value;

                        //password validation
                        $upperCaseRegEx = '@[A-Z]@';
                        $lowerCaseRegEx = '@[a-z]@';
                        $numberRegEx = '@[0-9]@';
                        $minCount = 8;

                        $upperCase = preg_match($upperCaseRegEx, $password);
                        $lowerCase = preg_match($lowerCaseRegEx, $password);
                        $number    = preg_match($numberRegEx, $password);

                        if (!$upperCase || !$lowerCase || !$number || strlen($password) < $minCount) {
                            return 'Password should be at least 8 characters in length and should include at least one upper case letter and one number.';
                        } else {
                            return true;
                        }
                    }
                ]
            );

        $validator
            ->scalar('profile_image')
            ->maxLength('profile_image', 255)
            ->allowEmptyFile('profile_image');

        $validator
            ->boolean('is_activated')
            ->allowEmptyString('is_activated');

        $validator
            ->boolean('is_verified')
            ->allowEmptyString('is_verified');

        $validator
            ->boolean('is_private')
            ->allowEmptyString('is_private');

        $validator
            ->scalar('password_hash')
            ->maxLength('password_hash', 100)
            ->allowEmptyString('password_hash');

        $validator
            ->dateTime('created_datetime')
            ->allowEmptyDateTime('created_datetime');

        $validator
            ->dateTime('modified_datetime')
            ->allowEmptyDateTime('modified_datetime');

        $validator
            ->dateTime('deleted_datetime')
            ->allowEmptyDateTime('deleted_datetime');

        $validator
            ->notEmptyString('confirm_password', 'Please fill this field.')
            ->add('confirm_password', 'error', [
                'rule' => ['compareWith', 'password'],
                'message' => 'Passwords are not equal',
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['email']), ['errorField' => 'email']);
        $rules->add($rules->isUnique(['username']), ['errorField' => 'username']);

        return $rules;
    }
}
