<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FollowersFollowingTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FollowersFollowingTable Test Case
 */
class FollowersFollowingTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FollowersFollowingTable
     */
    protected $FollowersFollowing;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.FollowersFollowing',
        'app.Users',
        'app.Follows',
        'app.Followings',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('FollowersFollowing') ? [] : ['className' => FollowersFollowingTable::class];
        $this->FollowersFollowing = $this->getTableLocator()->get('FollowersFollowing', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->FollowersFollowing);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\FollowersFollowingTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\FollowersFollowingTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
